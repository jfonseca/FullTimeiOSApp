# Full-time-iOS-App

This job finding application was developed and designed by a friend (Nor Sanavongsay) and I to build a better connection between talent and availible jobs in the tech industry. Utilizing Cameron Moll Authentic Jobs Site API's the application provided tech industry with an easy way to filter, schedule, apply, share on social and save availible jobs every where they went. This application is no longer availible in the Apple store but glad to be able to share the code with you on GIT

API provided by [Authentic Jobs] https://authenticjobs.com

Design/Developer [Nor Sanvongsay] http://nor.sanavongsay.com/portfolio/

Developer [Jose Fonseca] https://www.klipartstudio.com

[View Example Online](http://klipartstudio.com/site/AjobsApp/)

## What you will find in this repo

* _src/ : In here, you'll find all the Fireworks files Nor used to make the assets minus some copyrighted stuff. ;) 
* _src/_inspiration/ : Here's where Nor got inspiration from for the textures.
* Full-time_1.0.0/ : This is where the magic happens.

Feel free to contact Nor or I(Jose) if you have questions.

[Jose Fonseca] (josef@klipartstudio.com)

[Nor Sanavongsay] (artofnor@gmail.com)

### Developed in 2010 - 2012

## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/FullTimeiOSApp.git)